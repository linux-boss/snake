package game.snake;

public class Snake {
    private final Chane[] body;
    private int lengthBody;
    private int head;

    public Snake(int maxLength) {
        body = new Chane[maxLength];
        for (int i = 0; i < maxLength; i++) {
            body[i] = new Chane();
        }
        lengthBody = 1;
        head = 0;
        body[head] = new Chane(360, 280);
    }

    public Chane getChane(int i) {
        int tmp = head - i;
        if (tmp < 0) {
            tmp = body.length + tmp;
        }
        return body[tmp];
    }

    public int getHeadX() {
        return body[head].getX();
    }

    public int getHeadY() {
        return body[head].getY();
    }

    public int getLength() {
        return lengthBody;
    }

    public void setLength(int length) {
        lengthBody = length;
    }

    public void setHeadX(int x) {
        int tmp = head++;
        if (head > body.length - 1) {
            head = 0;
        }
        body[head].setX(x);
        body[head].setY(body[tmp].getY());
    }

    public void setHeadY(int y) {
        int tmp = head++;
        if (head > body.length - 1) {
            head = 0;
        }
        body[head].setY(y);
        body[head].setX(body[tmp].getX());
    }

    public static class Chane {
        private int x;
        private int y;

        public Chane() {

        }

        public Chane(int x, int y) {
            this.x = x;
            this.y = y;
        }

        public int getX() {
            return x;
        }

        public void setX(int x) {
            this.x = x;
        }

        public int getY() {
            return y;
        }

        public void setY(int y) {
            this.y = y;
        }
    }
}
