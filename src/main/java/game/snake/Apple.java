package game.snake;

public class Apple {
    int x;
    int y;

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public Apple() {
        locate();
    }

    public void locate() {
        int rnd = (int) (Math.random() * 20);
        this.x = rnd * 40;
        rnd = (int) (Math.random() * 15);
        this.y = rnd * 40;
    }
}
