package game.snake;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.util.Objects;
import java.net.URL;

public class Board extends JPanel implements Runnable {
    private final int B_WIDTH = 800;
    private final int B_HEIGHT = 600;
    private final int DELAY = 400;

    private Snake snake;
    private Apple apple;
    private BufferedImage background;
    private boolean upDirection;
    private boolean downDirection;
    private boolean leftDirection;
    private boolean rightDirection;
    private boolean inGame;
    private Image appleImage;
    private Image headImage;
    private Image chaneImage;


    public Board() {
        initBoard();
    }

    private void initBoard() {
        addKeyListener(new TAdapter());
        setFocusable(true);
        try (InputStream backgroundImg = getClass().getResourceAsStream("/background/grass.png")) {
            background = ImageIO.read(Objects.requireNonNull(backgroundImg));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        setPreferredSize(new Dimension(B_WIDTH, B_HEIGHT));

        loadImage();
        initGame();
    }

    @Override
    public void addNotify() {
        super.addNotify();
        Thread animator = new Thread(this);
        animator.start();
    }

    private void loadImage() {
        URL imgURL = getClass().getResource("/sprite/apple-40px.png");
        ImageIcon iia = new ImageIcon(Objects.requireNonNull(imgURL));
        appleImage = iia.getImage();
        imgURL = getClass().getResource("/sprite/head-40px.png");
        ImageIcon iih = new ImageIcon(Objects.requireNonNull(imgURL));
        headImage = iih.getImage();
        imgURL = getClass().getResource("/sprite/chane-40px.png");
        ImageIcon iic = new ImageIcon(Objects.requireNonNull(imgURL));
        chaneImage = iic.getImage();
    }

    private void initGame() {
        upDirection = true;
        downDirection = false;
        leftDirection = false;
        rightDirection = false;
        inGame = true;
        snake = new Snake(300);
        apple = new Apple();
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        doDrawing(g);
    }

    private void doDrawing(Graphics g) {
        if (inGame) {
            g.drawImage(background, 0, 0, this);
            g.drawImage(appleImage, apple.getX(), apple.getY(), this);

            g.drawImage(headImage, snake.getHeadX(), snake.getHeadY(), this);
            for (int i = 1; i < snake.getLength(); i++) {
                g.drawImage(chaneImage, snake.getChane(i).getX(), snake.getChane(i).getY(), this);
            }
            Toolkit.getDefaultToolkit().sync();
        } else {
            gameOver(g);
        }
    }

    private void gameOver(Graphics g) {
        String msgGameOver = "Game Over";
        String msgExitOrRepeat = "Press <Esc> to exit or any key to repeat";
        String msgLength = "Snake length: " + snake.getLength();
        Font small = new Font("Helvetica", Font.BOLD, 14);
        FontMetrics fm = getFontMetrics(small);
        g.setColor(Color.white);
        setBackground(Color.BLACK);
        g.setFont(small);
        g.drawString(msgGameOver, (B_WIDTH - fm.stringWidth(msgGameOver)) / 2, B_HEIGHT / 4);
        g.drawString(msgLength, (B_WIDTH - fm.stringWidth(msgLength)) / 2, B_HEIGHT / 3);
        g.drawString(msgExitOrRepeat, (B_WIDTH - fm.stringWidth(msgExitOrRepeat)) / 2, B_HEIGHT / 2);
    }

    private void move() {
        if (leftDirection) {
            snake.setHeadX(snake.getHeadX() - 40);
        }
        if (rightDirection) {
            snake.setHeadX(snake.getHeadX() + 40);
        }
        if (upDirection) {
            snake.setHeadY(snake.getHeadY() - 40);
        }
        if (downDirection) {
            snake.setHeadY(snake.getHeadY() + 40);
        }
    }

    private void checkCollision() {
        if (snake.getLength() > 3) {
            for (int i = snake.getLength(); i > 0; i--) {
                if (snake.getHeadX() == snake.getChane(i).getX() &&
                        snake.getHeadY() == snake.getChane(i).getY()) {
                    inGame = false;
                    break;
                }
            }
        }
        if ((snake.getHeadX() >= 800) | (snake.getHeadX() < 0)) {
            inGame = false;
        }
        if ((snake.getHeadY() >= 600) | (snake.getHeadY() < 0)) {
            inGame = false;
        }
    }

    private void checkApple() {
        if (snake.getHeadX() == apple.getX() && snake.getHeadY() == apple.getY()) {
            snake.setLength(snake.getLength() + 1);
            apple.locate();
        }
    }

    @Override
    public void run() {
        long beforeTime;
        long timeDiff;
        long sleep;

        beforeTime = System.currentTimeMillis();

        while (true) {
            if (inGame) {
                checkApple();
                checkCollision();
                move();
            }
            repaint();

            timeDiff = System.currentTimeMillis() - beforeTime;
            sleep = DELAY - timeDiff;

            if (sleep < 0) {
                sleep = 2;
            }
            try {
                Thread.sleep(sleep);
            } catch (InterruptedException e) {
                String msg = String.format("Thread interrupted: %s", e.getMessage());
                JOptionPane.showMessageDialog(this, msg, "Error",
                        JOptionPane.ERROR_MESSAGE);
            }
            beforeTime = System.currentTimeMillis();
        }
    }

    // Обработчик нажатия на клавиши
    private class TAdapter extends KeyAdapter {
        @Override
        public void keyPressed(KeyEvent e) {
            int key = e.getKeyCode();
            if (!inGame) {
                if (key == KeyEvent.VK_ESCAPE) {
                    System.exit(0);
                } else {
                    initGame();
                }
            }
            if ((key == KeyEvent.VK_LEFT) && (!rightDirection)) {
                leftDirection = true;
                upDirection = false;
                downDirection = false;
            }
            if ((key == KeyEvent.VK_RIGHT) && (!leftDirection)) {
                rightDirection = true;
                upDirection = false;
                downDirection = false;
            }
            if ((key == KeyEvent.VK_UP) && (!downDirection)) {
                upDirection = true;
                leftDirection = false;
                rightDirection = false;
            }
            if ((key == KeyEvent.VK_DOWN) && (!upDirection)) {
                downDirection = true;
                leftDirection = false;
                rightDirection = false;
            }
        }
    }
}